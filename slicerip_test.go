// slicerip rips slices out of strings and is writtin in the Go programming
// language.
// Copyright (C) 2017 J. Hartsfield

// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Package slicerip is a tool used for ripping slices out of strings so that
// they can be tested against encrypted data to programmatically ensure that
// the data was properly encrypted. It's mainly for use in tests of crypto
// code. This is the test file for it.
package slicerip

import (
	"strings"
	"testing"

	"gitlab.com/hartsfield/slicerip"
)

var (
	// Original string
	str = "The quick brown fox jumps over the lazy dog"
	// Slice up our original string into reasonably sized bits
	strSlice = slicerip.Extract(str, 3, 10)
)

// TestExtractNotFound tests the slice extraction function used in the slicerip
// package against a string which contains no substrings matching any of our
// extracted strings.
func TestExtractNotFound(t *testing.T) {
	// Random data representing encrypted data that doesn't contain any parts
	// of the original string greater than length 2.
	tstr1 := "cdisocdjs tidjsoicj odsjcoijewj[09ej revoire[vwjr0 ]]"

	// Iterate over the strSlice and use strings.Contains to test if any of the
	// slices are contained within the strings.
	for _, v := range strSlice {
		if strings.Contains(tstr1, v) { // should never return true
			t.Error(`FAILED`)
			return
		}
	}
}

// TestExtractFound tests the slice extraction function used in the slicerip
// package against a string which contains a substring matching some of our
// extracted strings.
func TestExtractFound(t *testing.T) {
	// Random data representing encrypted data that contains a small part of the
	// original string (brown fox)
	tstr2 := "iusocdjs tibrown foxicj odsjcoijewj[09ej revoire[vwjr0 ]]"

	// This iteration loop was separated into another function for better
	// readability/cleaner code.
	if !ite(strSlice, tstr2) {
		t.Error(`error2`)
	}
}

func ite(strSlice []string, tstr string) bool {
	// Iterate over the strSlice and use strings.Contains to test if any of the
	// slices are contained within the strings.
	for _, v := range strSlice {
		if strings.Contains(tstr, v) { // should return true sometimes
			return true
		}
	}
	return false
}

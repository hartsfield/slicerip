// slicerip rips slices out of strings and is writtin in the Go programming
// language.
// Copyright (C) 2017 J. Hartsfield

// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version.

// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.

// You should have received a copy of the GNU General Public License along with
// this program; if not, write to the Free Software Foundation, Inc., 51
// Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Package slicerip is a tool used for ripping slices out of strings so that
// they can be tested against encrypted data to programmatically ensure that
// the data was properly encrypted. It's mainly for use in tests of crypto
// code.
package slicerip

// Extract extracts slices from the given string. The min and max are the
// minimum and maximum length of a subslice. Small subslices increase the
// chances of a false positive.
func Extract(str string, min int, max int) []string {
	strSlice := []string{}
	for k, _ := range strSlice {
		for j := min; j <= max; j++ {
			strSlice = append(strSlice, str[k:k+j])
		}
	}
	return strSlice
}

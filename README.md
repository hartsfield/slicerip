<h1>slicerip</h1>
A Go package for ripping slices out of strings for comparison to encrypted 
data.

The purpose of this package is for use in crypto tests to try to find obvious
encryption errors. Many encryption methods require the data to be encrypted in
blocks. Go uses different mode implementations to encrypt blocks of data in
streams. This package rips substrings out of strings of data and returns them
in an array that can be used with `strings.Contains` to check the encrypted
data for portions of the original data.

Only 10 lines of code (without comments).

<h1>Example Usage:</h1>
(See the test package for even better examples, and 
https://gitlab.com/hartsfield/gencrypt/blob/master/gencrypt_test.go for use in
crypto tests)

```go
package main

import (
  "strings"

  "gitlab.com/hartsfield/slicerip"
)

var (
  // Original string
  str = "The quick brown fox jumps over the lazy dog"
  // Slice up our original string into reasonably sized bits, 3 is the minimum
  // and 10 is the maximum.
  strSlice = slicerip.Extract(str, 3, 10)
)

func main() {
  // Random data representing encrypted data that doesn't contain any parts of 
  // the original string greater than length 2.
  tstr1 := "cdisocdjs tidjsoicj odsjcoijewj[09ej revoire[vwjr0 ]]"
  // Random data representing encrypted data that contains a small part of the
  // original string (brown fox)
  tstr2 := "iusocdjs tibrown foxicj odsjcoijewj[09ej revoire[vwjr0 ]]"

  // Iterate over the strSlice and use strings.Contains to test if any of the
  // slices are contained within the strings.
  for _, v := range strSlice {
	if strings.Contains(tstr1, v) { // should never return true
      fmt.Println("You should never see this")
	}
	if strings.Contains(tstr2, v) { // should return true sometimes
      fmt.Println("This string contained a substring found in the original data")
      return
    }
  }
}
```
